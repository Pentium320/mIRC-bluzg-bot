## mIRC-bluzg-bot

## ABOUT
This repository contains bunch of scripts and utilities for mIRC bot. Scripts were originally made (or found yeaaars ago somewhere) for bot called "Bluzg" on #bluzgi @ freenode IRC network, but thought, that I can share a bit because hey - why not? Feel free to use it.

## CURRENT SCRIPTS
To be written later.

## COMPATIBILITY
I write and test scripts on:
- Windows Server 2008 R2 & mIRC 7.1
- Arch Linux x64 & Wine (Experimental) & mIRC 7.1

Works for me, so I guess, at every mIRC 7.x client should also.

## THANKS TO
Andrio from Esper.net

Always answered my newbie questions and helped with things, that I didn't understand in mIRC scripting :D

Iyouboushi from Esper.net

Helped me a lot with parsing script (well, almost wrote whole), and created BattleArena mIRC bot, which - when analyzing it also gave me some ideas.

People from mIRC forums

For bunch of "ready to use" solutions. Hard to mention nicks :)
