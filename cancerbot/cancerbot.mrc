; ustaw lokalizację bazy raka
alias rak.local return rak.txt

; ile % do holokaustu raka?
alias rak.holocaust return 10

; powód kicka?
alias rak.kick return rakotwórca

; a dalej nie ruszać, bo się zjebie
;----------------------------------

alias rak.procent {
  %procent = %rakcounter / %linecounter
  %procent = %procent * 100
  return %procent
}

on *:TEXT:*:#: {
  if ((%rakcounter !isnum) || (%linecounter !isnum)) {
    set %linecounter 0
    set %rakcounter 0
    set %holocaust 0
  }
  %linecounter = %linecounter + 1
  if ($1 == !rakcounter) {
    msg # Rak na kanale padł %rakcounter razy w %linecounter liniach, co daje stężenie raka wynoszące $rak.procent $+ $chr(37)
    halt
  }
  var %i = $lines($rak.local)
  var %answer = $chr(62)
  while (%i != 0) {
    %temp = $read($rak.local, n, %i)
    %reg = / $+ %temp $+ /g
    %x = $regex(name,$1-,%reg)
    if (%x > 0) {
      var %answer = %answer $+ $chr(32) $+ %temp
      %rakcounter = %rakcounter + %x
    }
    %i = %i - 1
  }
  if (%answer != $chr(62)) {
    msg # %answer $chr(124) RAK ALERT
    if (%holocaust == 1) {
      kick $chan $nick $rak.kick
    }
    unset %answer
  }
  if ($rak.procent > $rak.holocaust && %holocaust == 0) {
    msg # Stężenie raka przekroczyło $rak.holocaust $+ $chr(37). Ogłaszam holokaust! Następny rak będzie tępiony!
    set %holocaust 1
  }
  else if ($rak.procent <= $rak.holocaust && %holocaust == 1) {
    msg # Stężenie raka spadło. Holokaust odwołany.
    set %holocaust 0
  }
  unset %temp
  unset %reg
  unset %x
}

on *:KICK:#:/join $chan

on *:connect: {
  ignore -tw *
  debug -i debugoutput fakectcp
  fakectcpversion
}

alias fakectcp {
  tokenize 32 $1-
  if ($1 == <-) && ($3 == PRIVMSG) && (:* iswm $5-) && ($mid($5-,3,-1) == VERSION) {
    ctcpreply $mid($gettok($2,1,33),2) $v1 %fakectcpshow 
    fakectcpversion 
  }
}

alias fakectcpversion {
  set %fakectcpshow nie.
}
