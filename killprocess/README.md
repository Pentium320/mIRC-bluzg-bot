## killprocess
This is slightly modified script, used for killing processes via mIRC.

## USAGE
/killprocess <0/1> <process>

use 0 to kill all processes.

use 1 to kill first instance of process.