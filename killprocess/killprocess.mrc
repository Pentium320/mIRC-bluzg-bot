alias killprocess {
  var %e = echo -ac info * /killprocess:
  if ($1 !isnum 0-) || ($0 < 2) { %e <N> <process> with N zero or higher | return }
  var %a = a $+ $ticks, %b = b $+ %a, %c
  .comopen %a WbemScripting.SWbemLocator 
  if ($comerr) { %e error connecting to WMI | return }
  .comclose %a $com(%a,ConnectServer,1,dispatch* %b) 
  if ($com(%b)) .comclose %b $com(%b,ExecQuery,1,bstr*,SELECT $&
    Name FROM Win32_Process WHERE Name = $+(",$2-,"),dispatch* %a) 
  if (!$com(%a)) { %e error retrieving collection | return }
  %c = $comval(%a,0)
  if (!%c) { %e no such process $2 | return }
  if (!$1) {
    while (%c) {
      !.echo -q $comval(%a,%c,Terminate) 
      dec %c 
    } 
  }
  else !.echo -q $comval(%a,$1,Terminate) 
  :error
  if ($com(%a)) .comclose %a
  if ($com(%b)) .comclose %b
}
