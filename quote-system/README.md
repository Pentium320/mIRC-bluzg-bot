## quote-system

## ABOUT
This is simple script, like "bash.org" which keeps quotes from channel. Well, I guess it can be also used for other purposes.

## TRANSLATIONS
- English
- Polish

And more can be easily added.

## COMMANDS
"!q" is default prefix for this script, but this can be easily changed. Below commands are for !q prefix:
- !q [number/text] - additional parameters in brackets are optional
	- When used with number (ex. !q 1) will return quote with this number.
	- When used with text (ex. !q bread) will return quote with this word, or list quotes which contains this word (if more than 1 contains).
	- Without parameter returns random quote.
- !qadd [text] - adds new quote with last number
- !qdel [number] - removes quote with this number. After removing, every next quote number will decrease.
- !qedit [number] [text] - updates quote with this number with new text.
- !qcount - counts quotes in database
- !qlast - show last added quote
- !qhistory - shows last actions done in quote base.
- !qvote [number] [+/-] - voting for a quote, + for upvote, - for downvote

## CONFIG
Things you can set:
- Currently used translation
- Command prefix
- Path to quotes
- Path to history (you probably don't want to see this file anyway)
- Path to votes (you probably don't want to see this file anyway)
- Set amount of operations kept in history
- Check if only operator can add quotes
- Check if only operator can delete quotes
- Check if only operator can edit quotes

## KNOWN ISSUES
Nothing, for now.