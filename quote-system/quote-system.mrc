; Choose file with translation
alias quote_trans return translations\en.txt

; Command to use as prefix
alias quote_pref return !q

; Path to file with quotes
alias quote_file return quotes.txt

; Path to file with history
alias quote_history_file return history.txt

; Path to file with votes
alias quote_votes_file return votes.txt

; How many operations to keep in history? [0 means turned off] (note, that setting it too high can cause bot to get kicked from server for flood)
alias quote_history_oper return 5

; Can non-operator add quotes? (1: Yes, 0: No)
alias quote_checkop_add return 0

; Can non-operator delete quotes? (1: Yes, 0: No)
alias quote_checkop_del return 0

; Can non-operator edit quotes? (1: Yes, 0: No)
alias quote_checkop_edit return 0

;----------------
; MAIN FUNCTION
; (In other words, don't touch anything below, till you don't know what you're doing)
;----------------

on *:TEXT:*:#: {
  if ($1 == $quote_pref $+ add) {
    if (($quote_checkop_add == 0) && ($nick !isop $chan)) {
      msg # $read($quote_trans,3)
      halt
    }
    set %quote.temp $2-
    set %quote.nick $nick
    $quote_add
  }
  else if ($1 == $quote_pref $+ del) {
    if (($quote_checkop_del == 0) && ($nick !isop $chan)) {
      msg # $read($quote_trans,3)
      halt
    }
    set %quote.temp $2-
    set %quote.nick $nick
    $quote_del
  }
  else if ($1 == $quote_pref $+ edit) {
    if (($quote_checkop_edit == 0) && ($nick !isop $chan)) {
      msg # $read($quote_trans,3)
      halt
    }
    set %quote.temp $2
    set %quote.temptext $3-
    set %quote.nick $nick
    $quote_edit
  }
  else if ($1 == $quote_pref) {
    set %quote.temp $2-
    set %quote.nick $nick
    $quote_q
  }
  else if ($1 == $quote_pref $+ last) { $quote_last }
  else if ($1 == $quote_pref $+ count) { $quote_count }
  else if ($1 == $quote_pref $+ history) {
    set %quote.nick $nick
    $quote_history
  }
  else if ($1 == $quote_pref $+ vote) {
    set %quote.nick $nick
    set %quote.host $address(%quote.nick,0)
    set %quote.temp $2
    set %quote.char $3
    $quote_vote
  }
  unset %quote.temp
}

;----------------
; ALIASES
;----------------

alias quote_add {
  if (%quote.temp) {
    if ($chr(36) isin %quote.temp) {
      msg # $read($quote_trans,4)
    }
    else {
      set %quote.history.oper +
      write $quote_votes_file 0
      write $quote_votes_file hosts:
      write $quote_file %quote.temp
      set %quote.temp $lines($quote_file)
      $quote_history_write
      msg # $read($quote_trans,5)
    }
  }
  else {
    msg # $read($quote_trans,10)
  }
}

alias quote_del {
  if (%quote.temp isnum && %quote.temp > 0) {
    if ($read($quote_file,%quote.temp) != $null) {
      set %quote.history.oper -
      var %quote.i = 0
      while (%quote.i <= 1) {
        write -dl $calc(%quote.temp * 2 - 2) $quote_votes_file
        %quote.i = %quote.i + 1
      }
      write -dl %quote.temp $quote_file
      $quote_history_write
      msg # $read($quote_trans,6)
    }
    else {
      msg # $read($quote_trans,1)
    }
  }
  else {
    msg # $read($quote_trans,7)
  }
}

alias quote_edit {
  if (%quote.temp isnum && %quote.temptext && %quote.temp > 0) {
    if ($lines($quote_file) < %quote.temp) {
      msg # $read($quote_trans,1)
    }
    else {
      set %quote.history.oper !
      write -dl %quote.temp $quote_file
      write -al $calc(%quote.temp - 1) $quote_file $chr(13) $+ $chr(10) $+ %quote.temptext
      $quote_history_write
      msg # $read($quote_trans,12)
    }
  }
  else {
    msg # $read($quote_trans,11)
  }
  unset %quote.temptext
}

alias quote_q {
  if (%quote.temp) {
    if (%quote.temp isnum) {
      if ($read($quote_file,%quote.temp) != $null) {
        $quote_answer
      }
      else {
        msg # $read($quote_trans,1)
      }
    }
    else {
      var %quote.i = $lines($quote_file)
      var %quote.counter = 0
      .fopen quote_file $quote_file
      while (%quote.i != 0) {
        .fseek -l quote_file %quote.i
        var %quote.reg = / $+ $fread(quote_file) $+ /g
        if ($regex(name,%quote.reg,%quote.temp) > 0) {
          var %quote.x = %quote.x $+ $chr(32) $+ %quote.i
          %quote.counter = %quote.counter + 1
        }
        %quote.i = %quote.i - 1
      }
      .fclose quote_file
      if (%quote.counter == 1) {
        %quote.x = $replace(%quote.x, $chr(32), )
        %quote.temp = %quote.x
        $quote_answer
      }
      else if (%quote.counter == 0) {
        msg # $read($quote_trans,2)
      }
      else {
        msg # $read($quote_trans,9)
      }
    }
  }
  else {
    set %quote.temp $rand(1,$lines($quote_file))
    $quote_answer
  }
}

alias quote_history {
  var %quote.history.lines = $lines($quote_history_file)
  if ($quote_history_oper == 0 || $lines($quote_history_file) == 0) {
    msg # $read($quote_trans,2)
  }
  else {
    msg # $read($quote_trans,13)
    msg %quote.nick --== (+): $quote_pref $+ add *** (-): $quote_pref $+ del *** (!): $quote_pref $+ edit ==--
    var %quote.i = 0
    while (%quote.i != %quote.history.lines) {
      %quote.i = %quote.i + 1
      timer 1 %quote.i msg %quote.nick $read($quote_history_file, n, %quote.i)
    }
  }
}

alias quote_history_write {
  if ($quote_history_oper != 0) {
    write $quote_history_file ( $+ %quote.history.oper $+ ) $time $date -> %quote.nick -> $chr(35) $+ %quote.temp
    if ($lines($quote_history_file) > $quote_history_oper) {
      write -dl 1 $quote_history_file
    }
  }
}

alias quote_last {
  set %quote.temp $lines($quote_file)
  $quote_answer
}

alias quote_count {
  msg # $read($quote_trans,8)
}

alias quote_vote {
  if (%quote.temp isnum && %quote.temp > 0 && (%quote.char == $chr(43) || %quote.char == $chr(45))) {
    if ($read($quote_file,%quote.temp) != $null) {
      %quote.host = $remove(%quote.host, /, *)
      var %quote.reg = / $+ $read($quote_votes_file, n, $calc(%quote.temp * 2 - 1)) $+ /g
      if ($regex(host,%quote.reg,%quote.host) > 0 || $regex(hostnick,%quote.reg,%quote.nick) > 0) {
        msg # $read($quote_trans,16)
      }
      else {
        var %quote.mark = $read($quote_votes_file,$calc(%quote.temp * 2 - 2))
        write -dl $calc(%quote.temp * 2 - 1) $quote_votes_file
        if (%quote.char == $chr(43)) {
          write -il $calc(%quote.temp * 2 - 1) $quote_votes_file $calc(%quote.mark + 1)
        }
        else {
          write -il $calc(%quote.temp * 2 - 1) $quote_votes_file $calc(%quote.mark - 1)
        }
        var %quote.temptext = $read($quote_votes_file,$calc(%quote.temp * 2 - 1)) %quote.host %quote.nick
        write -dl $calc(%quote.temp * 2) $quote_votes_file
        write -il $calc(%quote.temp * 2) $quote_votes_file %quote.temptext
        msg # $read($quote_trans,15)
      }
    }
    else {
      msg # $read($quote_trans,1)
    }
  }
  else {
    msg # $read($quote_trans,14)
  }
}

alias quote_answer {
  var %quote.mark = $read($quote_votes_file,$calc(%quote.temp * 2 - 2))
  if (%quote.mark < 0) {
    %quote.mark = 4 $+ %quote.mark $+ 
  }
  else if (%quote.mark > 0) {
    %quote.mark = 3 $+ $chr(43) $+ %quote.mark $+ 
  }
  msg #  $+ $chr(35) $+ %quote.temp $+ $chr(91) $+ %quote.mark $+ $chr(93) $+ : $read($quote_file,%quote.temp)
}
